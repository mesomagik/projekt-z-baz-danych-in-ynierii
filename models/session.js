/**
 * Created by jacek on 27.05.15.
 */

var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;

module.exports = Object.create(Model, {
    table: {value: 'sesja'},
     create:{
        value: function (sesja, callback) {
            var that = this;
            this.selectMaxInColumn('id_sesja', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    sesja.id_sesja = 0;
                else
                    sesja.id_sesja = max + 1;
                that.insert(sesja, function (result) {
                    callback(sesja.id_sesja);
                });
            });
        }
    },
    getSessions:{
        value: function (callback) {
            this.connection.query('select * from sesja',
            function (err, rows, fields)
            {
                console.log(rows);
                callback(rows);
            });
        
        }
    },
    selectAll: {
        value: function (callback) {
        this.connection.query('SELECT * FROM ??', [this.table], function (err, rows, fields) {
            if (err)
                throw err;
            else
                callback(rows);
        });
    }
    }
});
