var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;

module.exports = Object.create(Model, {
    table: {value: 'przypisanie'},
    registerForUser: {
        value: function(obj, callback) {
            var that = this
            this.selectMaxInColumn('id_przypisanie', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    obj.id_przypisanie = 0;
                else
                    obj.id_przypisanie = max + 1;
                that.insert(obj, function() {
                    callback(obj.id_przypisanie);
                }) 
            });
        }
    }
});
