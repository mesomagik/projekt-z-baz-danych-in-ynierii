/**
 * Created by jacek on 27.05.15.
 */
var ErrorCode = require('./model').ErrorCode;
var Model = require('./model').Model;

module.exports = Object.create(Model, {
    table: {value: 'recenzja'},

    //miejsce na funkcje
    selectCount: function (callback) {
        this.connection.query(
            'SELECT COUNT(*) FROM ??',
            [this.table], function (err, rows, fields) {
                var liczba = query;
                if (err)
                    throw err;
                else
                    callback(liczba);
            });
    },
    add: {
        value: function (review, callback) {
            var that = this;
            this.selectMaxInColumn('id_recenzja', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    review.id_recenzja = 0;
                else
                    review.id_recenzja = max + 1;
                that.insert(review, function (result) {
                    callback(review.id_recenzja);
                });
            });
        }
    },
     SelectAllReviews: 
    {
        value:function(callback){
            this.connection.query('select u.imie as imie, u.nazwisko as nazwisko, rn.nazwa as nazwar, rf.nazwa as nazwarf, k.nazwa as nazwak from uzytkownik u, recenzja rn, referat rf, konferencja k, uczestnik ucz  where u.id_uzytkownika=ucz.uzytkownik_id_uzytkownika and rf.uczestnik_id_uczestnik=ucz.id_uczestnik and rf.id_referat=rn.referat_id_referat and rf.konferencja_id_konferencja=k.id_konferencja', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
        });
      }
    },
    
     deleteReview: {
        value: function (id,callback) {
        this.connection.query('DELETE FROM ?? WHERE id_recenzja = ?',
            [this.table ,id], function (err, result) {
                if (err) throw err;
                else
                callback();
            }
            );
        }
    },
    
    
    
    getAllForReviewer: {
        value: function(user_id, callback) {
            this.connection.query('SELECT r.id_recenzja, r.nazwa, COUNT(k.data) as ilosc_komentarzy FROM recenzja r LEFT JOIN komentarz k ON ' +
                                'k.recenzja_id_recenzja = r.id_recenzja INNER JOIN p_recenzenta p ON p.id_przypisania = r.p_recenzenta_id_przypisania AND ' + 
                                'p.uzytkownik_id_uzytkownika = ? GROUP BY r.id_recenzja',
            [user_id], function(err, rows, fields) {
                if(err)
                    throw err;
                callback(rows);
            });
        }
    }
});
