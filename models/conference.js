/**
 * Created by jacek on 2015-05-03.
 */


    var Model = require('./model').Model;
    var ErrorCode = require('./model').ErrorCode;
    
    
    module.exports = Object.create(Model, {
    table: {value: 'konferencja'},
    //miejsce na funkcje
    create:{
        value: function (conference, callback) {
            var that = this;
            this.selectMaxInColumn('id_konferencja', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    conference.id_konferencja = 0;
                else
                    conference.id_konferencja = max + 1;
                that.insert(conference, function (result) {
                    callback(conference.id_konferencja);
                                });
            });
        }
    },
    ShowConferences:{
    value : function(callback){
        this.connection.query('select k.id_konferencja as id_konferencja, k.nazwa as nazwa, k.data_rozpoczecia as data_rozpoczecia, k.data_zakonczenia as data_zakonczenia, k.adres as adres from konferencja k',
        function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    
            CreateCalendar: 
    {
        value:function(callback){
            this.connection.query('select u.imie as imie, u.nazwisko as nazwisko, rf.nazwa as nazwarf, s.nazwa as nazwas, k.nazwa as nazwak, k.data_rozpoczecia as data from uzytkownik u, uczestnik ucz, referat rf, sesja s, konferencja k where u.id_uzytkownika=ucz.uzytkownik_id_uzytkownika and ucz.id_uczestnik=rf.uczestnik_id_uczestnik and s.id_sesja=rf.sesja_id_sesja and k.id_konferencja=rf.konferencja_id_konferencja and u.uczestnik=1 and u.recenzent=0', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
        },
        
    ShowActiveConferences:{
    value : function(callback){
        this.connection.query('select k.nazwa as nazwa, k.adres as adres from konferencja k where k.aktywna=1 ',
        function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    ShowParticipants:{
     value : function(conference,callback){
         var key = conference;
         this.connection.query('select u.imie,u.nazwisko, u.email, h.nazwa from uzytkownik u, przypisanie p, hotel h, konferencja k where u.id_uzytkownika=p.uzytkownik_id_uzytkownika and h.id_hotel=hotel_id_hotel and k.id_konferencja=p.konferencja_id_konferencja and p.wplata=1 and k.id_konferencja=?',[conference.id_konferencja],
         function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
     }
    },
    AddIntoConference:{
        value : function(obj,callback){
            this.connection.query('insert into przypisanie set ?', [obj], function(err, result) {
                callback();
            })
        }
    },
    getParticipantConferences: {
        value: function(part_id, callback) {
            this.connection.query('select k.id_konferencja, k.nazwa, p.id_przypisanie from przypisanie p, konferencja k where p.uzytkownik_id_uzytkownika = ? ' + 
                                'and k.id_konferencja = p.konferencja_id_konferencja'
            , [part_id],
            function(err, rows, fields) {
                if(err)
                    throw err;
                callback(rows);
            })   
        }
    },
    getWhereNotParticipating: {
        value: function(part_id, callback) {
            this.connection.query('select k.id_konferencja, k.nazwa from konferencja k where ' + 
                                'k.id_konferencja NOT IN (SELECT konferencja_id_konferencja FROM przypisanie WHERE uzytkownik_id_uzytkownika = ?)'
            , [part_id],
            function(err, rows, fields) {
                if(err)
                    throw err;
                callback(rows);
            })   
        }
    },
    getAvailableParticipantConferences: {
        value: function(part_id, callback) {
            this.connection.query('select k.id_konferencja, k.nazwa, p.id_przypisanie from przypisanie p, konferencja k where p.uzytkownik_id_uzytkownika = ? ' + 
                                'and k.id_konferencja = p.konferencja_id_konferencja and p.id_przypisanie NOT IN (SELECT przypisanie_id_przypisanie FROM referat)'
            , [part_id],
            function(err, rows, fields) {
                if(err)
                    throw err;
                callback(rows);
            })   
        }
    },
    getAllForUser:{
        value: function(id, callback) {
                this.connection.query('select konferencja_id_konferencja from przypisanie where uzytkownik_id_uzytkownika = ?', [id],
                function(err, rows, fields) {
                    if(err)
                        throw err;
                    callback(rows);

                });
        }
    },
    getCharges: {
        value: function(callback) {
            this.connection.query('select * from oplaty',
            function(err, rows, fields) {
                if(err)
                    throw err;
                callback(rows);
            }) ;  
        }
    }




});
