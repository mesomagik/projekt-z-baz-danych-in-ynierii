/**
 * Created by jacek on 2015-05-03.
 */

var express = require('express');
var paper = require('../models/paper');
var review = require('../models/review');
var conference = require('../models/conference');

module.exports = function (passport) {
    var router = express.Router();

    router.get('/', function (req, res) {
        if(req.user && req.user.recenzent) {
            paper.getAllAssignedToReviewer(req.user.id_uzytkownika, function(papers) {
                review.getAllForReviewer(req.user.id_uzytkownika, function(reviews) {
                    res.render('reviewer/dashboard', {papers:papers, reviews:reviews});
                });
            });
        }
        else {
            conference.ShowActiveConferences(function (conf1) {
            res.render('index', { conference: conf1 });
            });
        }
    });
    return router;
};
